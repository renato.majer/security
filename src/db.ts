import { Pool } from "pg";
import dotenv from 'dotenv';
dotenv.config();

export type User = {
    username: string
    password: string,
    first_name: string,
    last_name: string,
    email: string,
    year_of_birth: number
}

const pool = new Pool ({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: 5432
   // ssl: true
})

export async function getUserDataVulnerable(username: string, password: string) {
    let query = `SELECT * FROM users_lab2 WHERE username='${username}' AND password='${password}';`
    console.log(query)
    const results = await pool.query(query)

    if(results.rowCount === 0) {
        throw Error("No data for given username and password")
    }

    return JSON.stringify(results.rows)
}

export async function getUserDataSecure(username: string, password: string) {
    const results = await pool.query('SELECT * FROM users_lab2 WHERE username=$1 AND password=$2;', [username, password])

    if(results.rowCount === 0) {
        throw Error("No data for given username and password")
    }

    return JSON.stringify(results.rows)
}
