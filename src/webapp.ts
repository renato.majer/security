import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import fs from 'fs';
import path from 'path';
import https from 'https';
import Tokens from 'csrf';
import * as db from './db';
import * as auth from './auth-middleware';

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

auth.initCookieAuth(app, 'login');

const tokens = new Tokens();

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

var isCSRFEnabled: boolean = false;
var userSecret: string | undefined = undefined;

app.get('/', async function(req, res) {

    let username = req.user;
    let token: string | undefined = undefined;

    if(username !== undefined && userSecret !== undefined) {
        // user is logged in
        token = tokens.create(userSecret) 
    }

    res.render('index', {username, token})
});


app.post('/', async function(req, res) {

    let isVulnerabilityEnabled: boolean = false;
    
    if(req.body["vulnerability-enabled"] !== undefined) { // checkbox is checked
        isVulnerabilityEnabled = true;
    }

    let username = req.body.username;
    let password = req.body.password;

    let results: string | undefined = undefined;
    let error: string | undefined = undefined;

    try {
        if(isVulnerabilityEnabled) {
            results = await db.getUserDataVulnerable(username, password)
    
        } else {

            if (typeof username != "string" || typeof password != "string") {
                throw Error("Invalid parameters!")
            }
    
            results = await db.getUserDataSecure(username, password)
        }

    } catch(err) {
        console.log(err.message)
        error = "Neispravno korisničko ime ili lozinka."
    }

    res.render('index', {results, error})
})

app.get('/login', function(req, res) {

    const username = req.user;
    const returnUrl = req.query.returnUrl

    if(username) {
        // user is already logged in -> redirect to home page
        res.redirect('/')
    } else {
        let error: string | undefined = undefined;
        if(returnUrl !== undefined) {
            // there is a returnUrl -> display appropriate message
            error = "Potrebno je ulogirati se prije pristupa traženoj stranici!"
        }
        res.render('login', {error})
    }
})

app.post('/login', function (req, res) {
    const username = req.body.username;
    const password = req.body.password;

    if(username !== 'admin' || password !== 'password') {
        res.render('login', {error: 'Neispravno korisničko ime ili lozinka.'})

    } else {

        auth.signInUser(res, username)

        userSecret = tokens.secretSync(); // create new secret on every login

        const returnUrl = req.query.returnUrl
        if(typeof returnUrl === 'string') {
            res.redirect(returnUrl)

        } else {
            
            res.redirect('/');
        }
    }
})


app.post('/logout', function (req, res) {

    if(!isCSRFEnabled) {
        // check token
        let token = req.body.token
        if(userSecret !== undefined && token !== undefined) {
            // secret is set and token is received
            if(!tokens.verify(userSecret, token)) {
                // token is not valid -> do not logout user
                res.status(403).end();
                return;
            }
        } else {
            // token not received or secret not set
            res.status(403).end();
            return;
        }
    }

    auth.signOutUser(res);
    userSecret = undefined; // reset secret value
    res.redirect('/')
})

app.get('/csrf', auth.requiresAuthentication, function(req, res) {

    let token: string | undefined = undefined

    if(userSecret !== undefined) {
        // user secret is set
        token = tokens.create(userSecret);
    }

    res.render('csrf', {isCSRFEnabled, token})
})

app.post('/csrf', auth.requiresAuthentication, function(req, res) {
    
    if(req.body["csrf-enabled"] !== undefined) { // checkbox is checked
        isCSRFEnabled = true;
    } else {
        isCSRFEnabled = false;
    }
    res.redirect('/csrf')
})

// start server
if (externalUrl) {
    const hostname = '0.0.0.0';
    app.listen(port, hostname, () => {
    console.log(`Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`);
    });

} else {
    https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.cert')
    }, app)
    .listen(port, function () {
        console.log(`Server running at https://localhost:${port}/`);
    });
}